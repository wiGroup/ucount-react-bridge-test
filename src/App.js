import React, { Component } from 'react'
import { Subject } from 'rxjs'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // session data to be updated from Subject message.
        sessionData: null,
        contextData: null
    }

    // setup subject on window.
      window.getSessionTokenSubject = new Subject()
      window.getContextTokenSubject = new Subject()

    // update method that can update this component instance when referenced from within a function()
    const updateState = newState => this.setState({ ...newState })

    window.getSessionTokenSubject.subscribe({
      next: function(data) {
        console.log(`Callback value from getSessionToken ${data}`)
        // update component state with our data payload
        updateState({ sessionData: data })
      },
    })

      window.getContextTokenSubject.subscribe({
          next: function(data) {
              console.log(`Callback value from getContext bridge ${data}`)
              // update component state with our data payload
              updateState({ contextData: data })
          },
      })
  }

  componentDidMount = () => {
    // created window callback
    window.getSessionTokenCallback = function (data) {
      console.log(`session callback data ${data}`)
      window.getSessionTokenSubject.next(data)
      return true
    }

      window.getContextCallback = function (data) {
          console.log(`context callback data ${data}`)
          window.getContextTokenSubject.next(data)
          return true
      }

    // window.getSessionTokenSubject.next('data')
  }

  invokeSessionBridge = () => {
    if (window.webkit && window.webkit.messageHandlers.SessionBridge) {
      console.log('postMessage for getSessionTokenCallback ios')
      window.webkit.messageHandlers.SessionBridge.postMessage(null)
    } else if (window.SessionBridge) {
      console.log('postMessage for getSessionTokenCallback android')
      window.SessionBridge.postMessage(null)
    }
  }


    invokeContextBridge = () => {
       
       if (window.webkit && window.webkit.messageHandlers.ContextBridge) {
            window.webkit.messageHandlers.ContextBridge.postMessage(null);
        } else if (window.ContextBridge) {
            console.log('postMessage for getContextBridgeCallback android')
            window.ContextBridge.postMessage(null);
        }
    }

    invokeDeepLinkBridge = () => {
        const deepLinkAction = {
            'actionType': 'browser',
            'metadata': {
                'url': 'https://www.standardbank.co.za'
            }
        };
        if (window.webkit && window.webkit.messageHandlers.DeepLinkingBridge) {
            window.webkit.messageHandlers.DeepLinkingBridge.postMessage(JSON.stringify(deepLinkAction));
        } else if (window.DeepLinkingBridge) {
            window.DeepLinkingBridge.postMessage(JSON.stringify(deepLinkAction));
        }
    }

    invokeTearDownBridge = () => {
        if (window.webkit && window.webkit.messageHandlers.TearDownBridge) {
            window.webkit.messageHandlers.TearDownBridge.postMessage(null);
        } else if (window.TearDownBridge) {
            window.TearDownBridge.postMessage(null);
        }
    }

    showPositiveMessage = () => {
            const messageAction = {
                'notificationType': 'success',
                'metadata': {
                    'message': 'a sunny message'
                }
            };
            if (window.webkit && window.webkit.messageHandlers.NotificationBridge) {
                window.webkit.messageHandlers.NotificationBridge.postMessage(JSON.stringify(messageAction));
            } else if (window.NotificationBridge) {
                window.NotificationBridge.postMessage(JSON.stringify(messageAction));
            }
        };

    showNegativeMessage = () => {
            const messageAction = {
                'notificationType': 'failure',
                'metadata': {
                    'message': 'a cloudy message'
                }
            };
            if (window.webkit && window.webkit.messageHandlers.NotificationBridge) {
                window.webkit.messageHandlers.NotificationBridge.postMessage(JSON.stringify(messageAction));
            } else if (window.NotificationBridge) {
                window.NotificationBridge.postMessage(JSON.stringify(messageAction));
            }
        };

    showNeutralMessage = () => {
            const messageAction = {
                'notificationType': 'neutral',
                'metadata': {
                    'message': 'a meh message'
                }
            };
            if (window.webkit && window.webkit.messageHandlers.NotificationBridge) {
                window.webkit.messageHandlers.NotificationBridge.postMessage(JSON.stringify(messageAction));
            } else if (window.NotificationBridge) {               
                window.NotificationBridge.postMessage(JSON.stringify(messageAction));
            }
        };
    
  render() {
    return (
      <>
        <h1>Awesome.</h1>
        <div style={{ display: 'block', marginTop: '20px' }}>
          {/* Render stringified response of the data payload from our Subject */}
          <h1>Session data: {JSON.stringify(this.state.sessionData)}</h1>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* Render stringified response of the data payload from our Subject */}
            <h1>Context data: {JSON.stringify(this.state.contextData)}</h1>
            </div>
        <div style={{ display: 'block', marginTop: '20px' }}>
          {/* This button called the invokeSessionBridge method defined above */}
          <button type='button' onClick={this.invokeSessionBridge}>Invoke Session Bridge</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.invokeContextBridge}>Invoke Context Bridge</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.invokeDeepLinkBridge}>Open native browser</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.invokeTearDownBridge}>Invoke teardown bridge</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.showPositiveMessage}>Show positive notification</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.showNegativeMessage}>Show negative notification</button>
            </div>

            <div style={{ display: 'block', marginTop: '20px' }}>
            {/* This button called the invokeSessionBridge method defined above */}
            <button type='button' onClick={this.showNeutralMessage}>Show neutral notification</button>
            </div>
      </>
    )
  }
}
